package com.example.americancitizenshiptest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.americancitizenshiptest.databinding.ActivityMainBinding;
import com.example.americancitizenshiptest.model.Question;
import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private int CurrentQuestionIndex = 0;
    private Question[] questionbank = new Question[]{
            new Question(R.string.question_amendments,false),
            new Question(R.string.question_constitution,true),
            new Question(R.string.question_declaration,true),
            new Question(R.string.question_independence_rights,true),
            new Question(R.string.question_religion,true),
            new Question(R.string.question_government,false),
            new Question(R.string.question_government_feds,false),
            new Question(R.string.question_government_senators,true)

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        binding.questionTextview.setText(questionbank[CurrentQuestionIndex].getAnswerResID());
        binding.trueButton.setOnClickListener(view ->
        {
            CheckAnswer(true);
        });
        binding.falseButton.setOnClickListener(view ->
        {
            CheckAnswer(false);
        });
        binding.nextButton.setOnClickListener(view -> {
            CurrentQuestionIndex = (CurrentQuestionIndex + 1) % questionbank.length;
            updateQuestion();

        });

        binding.prevButton.setOnClickListener(view -> {
            if(CurrentQuestionIndex > 0) {
                CurrentQuestionIndex = (CurrentQuestionIndex - 1) % questionbank.length;
                updateQuestion();
            }
        });
    }

    private void CheckAnswer(boolean userChosenAnswer)
    {
        boolean AnswerisCorrect = questionbank[CurrentQuestionIndex].isAnswerTrue();
        int messageID;

        if(AnswerisCorrect == userChosenAnswer)
        {
            messageID = R.string.correct_answer;
        }
        else
        {
            messageID = R.string.wrong_answer;
        }

        Snackbar.make(binding.imageView,messageID,Snackbar.LENGTH_SHORT).show();
    }

    private void updateQuestion() {
        binding.questionTextview.setText(questionbank[CurrentQuestionIndex].getAnswerResID());
    }
}